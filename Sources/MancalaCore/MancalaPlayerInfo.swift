//
//  MancalaPlayerInfo.swift
//  Mancala
//
//  Created by Michael Sanford on 3/15/17.
//  Copyright © 2017 flipside5. All rights reserved.
//

import NetworkingCore

public struct MancalaPlayerInfo: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    public let playerName: String
    public let gameboardID: String
    public let points: Int
    public let numberOfGamesPlayed: Int
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        return "{ \(playerName) | \(gameboardID) | \(points) | \(numberOfGamesPlayed)}"
    }
    
    public init(playerName: String, gameboardID: String, points: Int, numberOfGamesPlayed: Int) {
        self.playerName = playerName
        self.gameboardID = gameboardID
        self.points = points
        self.numberOfGamesPlayed = numberOfGamesPlayed
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            playerName = try unarchiver.decodeString(asAscii: false)
            gameboardID = try unarchiver.decodeString()
            points = try unarchiver.decodeInt()
            numberOfGamesPlayed = try unarchiver.decodeInt()
        } catch {
            log(.warning, "MancalaPlayerInfo", "Failed to archive MancalaPlayerInfo")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(playerName, asAscii: false)
        archiver.encode(gameboardID)
        archiver.encode(points)
        archiver.encode(numberOfGamesPlayed)
    }
}
