//
//  GamePieceType.swift
//  mac
//
//  Created by Sanford, Michael on 6/20/15.
//  Copyright © 2015 Sanford, Michael. All rights reserved.
//

import Foundation
import GamingCore
import NetworkingCore

public enum PlayerIdentity: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    case one, two
    
    public var opponentIdentity: PlayerIdentity {
        switch self {
        case .one: return .two
        case .two: return .one
        }
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        switch self {
        case .one: return "One"
        case .two: return "Two"
        }
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            let isPlayerTwo: Bool = try unarchiver.decodeBool()
            self = isPlayerTwo ? .two : .one
        } catch {
            assertionFailure("PlayerIdentity failed to unarchive")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        var isPlayerTwo = false
        if case .two = self {
            isPlayerTwo = true
        }
        archiver.encode(isPlayerTwo)
    }
}

extension GameToken {
    public var userType: UserType {
        switch playerID {
        case PlayerID.player1, PlayerID.player2: return .player(playerID)
        case PlayerID.spectator: return .spectator
        default:
            assertionFailure("Mancala player id is invalid (playerID=\(playerID))")
            return .spectator
        }
    }
}
