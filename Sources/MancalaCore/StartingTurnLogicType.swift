//
//  StartingGamePieceType.swift
//  mac
//
//  Created by Sanford, Michael on 6/20/15.
//  Copyright © 2015 Sanford, Michael. All rights reserved.
//

import NetworkingCore

fileprivate let oneText = "one"
fileprivate let twoText = "two"
fileprivate let toggleText = "toggle"
fileprivate let randomText = "random"

public enum StartingTurnLogicType: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    case one, two, toggle, random
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        switch self {
        case .one: return oneText
        case .two: return twoText
        case .toggle: return toggleText
        case .random: return randomText
        }
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            let string = try unarchiver.decodeString()
            switch string {
            case oneText: self = .one
            case twoText: self = .two
            case toggleText: self = .toggle
            case randomText: self = .random
            default:
                assert(false, "Failed to unarchive StartingTurnLogicType")
                self = .toggle
            }
        } catch {
            assert(false, "Failed to unarchive StartingTurnLogicType")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(description)
    }
}
