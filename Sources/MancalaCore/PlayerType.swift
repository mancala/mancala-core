//
//  PlayerType.swift
//  Mancala
//
//  Created by Michael Sanford on 6/26/15.
//  Copyright © 2016 flipside5. All rights reserved.
//

import NetworkingCore

fileprivate let humanText = "human"
fileprivate let botText = "bot"

public enum PlayerType: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    case human, bot(BotLevel)
    
    public var isHuman: Bool {
        switch self {
        case .human: return true
        case .bot: return false
        }
    }
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        switch self {
        case .human: return humanText
        case .bot(let level): return "bot:\(level.debugDescription)"
        }
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            let string = try unarchiver.decodeString()
            switch string {
            case humanText:
                self = .human
            case botText:
                guard let level = BotLevel.init(unarchiver: unarchiver) else {
                    assert(false, "Failed to unarchive PlayerType")
                    self = .bot(.intermediate)
                    break
                }
                self = .bot(level)
            default:
                assert(false, "Failed to unarchive PlayerType")
                return nil
            }
        } catch {
            assert(false, "Failed to unarchive PlayerType")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        switch self {
        case .human:
            archiver.encode(humanText)
        case .bot(let level):
            archiver.encode(botText)
            level.encode(with: archiver)
        }
    }
}

/*----------------------------------------------------*/

fileprivate let sillyText = "silly"
fileprivate let beginnerText = "beginner"
fileprivate let intermediateText = "intermediate"
fileprivate let advancedText = "advanced"
fileprivate let masterText = "master"
fileprivate let tutorialText = "tutorial"

public enum BotLevel: CustomStringConvertible, CustomDebugStringConvertible, ByteCoding {
    case silly, beginner, intermediate, advanced, master, tutorial
    
    public var description: String {
        return debugDescription
    }
    
    public var debugDescription: String {
        switch self {
        case .silly: return sillyText
        case .beginner: return beginnerText
        case .intermediate: return intermediateText
        case .advanced: return advancedText
        case .master: return masterText
        case .tutorial: return tutorialText
        }
    }
    
    public init?(unarchiver: ByteUnarchiver) {
        do {
            let string = try unarchiver.decodeString()
            switch string {
            case sillyText: self = .silly
            case beginnerText: self = .beginner
            case intermediateText: self = .intermediate
            case advancedText: self = .advanced
            case masterText: self = .master
            case tutorialText: self = .tutorial
            default:
                assert(false, "Failed to unarchive BotLevel")
                self = .intermediate
            }
        } catch {
            assert(false, "Failed to unarchive BotLevel")
            return nil
        }
    }
    
    public func encode(with archiver: ByteArchiver) {
        archiver.encode(description)
    }
}
