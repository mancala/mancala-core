import XCTest
@testable import mancala_coreTests

XCTMain([
    testCase(mancala_coreTests.allTests),
])
