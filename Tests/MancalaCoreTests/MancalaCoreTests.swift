import XCTest
import NetworkingCore
@testable import MancalaCore

class MancalaCoreTests: XCTestCase {
    
    func testPlayerInfoCoding() {
        let prePlayerInfo = MancalaPlayerInfo(
            playerName: "mike😮",
            gameboardID: "some.thing.board",
            points: 12345,
            numberOfGamesPlayed: 10
        )
        let archiver = ByteArchiver()
        prePlayerInfo.encode(with: archiver)
        let unarchiver = ByteUnarchiver(archive: archiver.archive)
        guard let postPlayerInfo = MancalaPlayerInfo.init(unarchiver: unarchiver) else {
            XCTAssert(false, "failed to unarchive MancalaPlayerInfo")
            return
        }
        XCTAssert(prePlayerInfo.playerName == postPlayerInfo.playerName, "player names do not match")
        XCTAssert(prePlayerInfo.gameboardID == postPlayerInfo.gameboardID, "gameboardIDs do not match")
        XCTAssert(prePlayerInfo.points == postPlayerInfo.points, "scores do not match")
    }
    
    func testPlayerIdentityCoding() {
        let prePlayer1 = PlayerIdentity.one
        let prePlayer2 = PlayerIdentity.two
        let archiver = ByteArchiver()
        prePlayer1.encode(with: archiver)
        prePlayer2.encode(with: archiver)
        let unarchiver = ByteUnarchiver(archive: archiver.archive)
        guard let postPlayer1 = PlayerIdentity.init(unarchiver: unarchiver),
            let postPlayer2 = PlayerIdentity.init(unarchiver: unarchiver) else {
            XCTAssert(false, "failed to unarchive PlayerIdentity's")
            return
        }
        XCTAssert(prePlayer1 == postPlayer1, "player1 ids do not match")
        XCTAssert(prePlayer2 == postPlayer2, "player2 ids do not match")
    }
    
    func testPlayerTypeCoding() {
        let preHuman = PlayerType.human
        let preBot1 = PlayerType.bot(.beginner)
        let preBot2 = PlayerType.bot(.intermediate)
        let preBot3 = PlayerType.bot(.advanced)
        
        let archiver = ByteArchiver()
        preHuman.encode(with: archiver)
        preBot1.encode(with: archiver)
        preBot2.encode(with: archiver)
        preBot3.encode(with: archiver)
        let unarchiver = ByteUnarchiver(archive: archiver.archive)
        guard let postHuman = PlayerType.init(unarchiver: unarchiver),
            let postBot1 = PlayerType.init(unarchiver: unarchiver),
            let postBot2 = PlayerType.init(unarchiver: unarchiver),
            let postBot3 = PlayerType.init(unarchiver: unarchiver) else {
                XCTAssert(false, "failed to unarchive PlayerType's")
                return
        }
        XCTAssert(preHuman == postHuman, "player1 ids do not match")
        XCTAssert(preBot1 == postBot1, "player1 ids do not match")
        XCTAssert(preBot2 == postBot2, "player1 ids do not match")
        XCTAssert(preBot3 == postBot3, "player1 ids do not match")
    }
    
    func testStartingLogicCoding() {
        let pre1 = StartingTurnLogicType.one
        let pre2 = StartingTurnLogicType.two
        let pre3 = StartingTurnLogicType.toggle
        let pre4 = StartingTurnLogicType.random
        
        let archiver = ByteArchiver()
        pre1.encode(with: archiver)
        pre2.encode(with: archiver)
        pre3.encode(with: archiver)
        pre4.encode(with: archiver)
        
        let unarchiver = ByteUnarchiver(archive: archiver.archive)
        
        guard let post1 = StartingTurnLogicType.init(unarchiver: unarchiver),
            let post2 = StartingTurnLogicType.init(unarchiver: unarchiver),
            let post3 = StartingTurnLogicType.init(unarchiver: unarchiver),
            let post4 = StartingTurnLogicType.init(unarchiver: unarchiver) else {
                XCTAssert(false, "failed to unarchive StartingTurnLogicType's")
                return
        }
        XCTAssert(pre1 == post1, "\(pre1.description) does not match")
        XCTAssert(pre2 == post2, "\(pre2.description) does not match")
        XCTAssert(pre3 == post3, "\(pre3.description) does not match")
        XCTAssert(pre4 == post4, "\(pre4.description) does not match")
    }
}

extension PlayerType: Equatable {}

public func ==(lhs: PlayerType, rhs: PlayerType) -> Bool {
    switch (lhs, rhs) {
    case (.human, .human): return true
    case (.bot(let lhsLevel), .bot(let rhsLevel)): return lhsLevel == rhsLevel
    default: return false
    }
}
