// swift-tools-version:4.0

import PackageDescription

let package = Package(
    name: "MancalaCore",
    products: [
        .library(
            name: "MancalaCore",
            targets: ["MancalaCore"])
    ],
    dependencies: [
        .package(url: "git@gitlab.com:mancala/gaming-core.git", from: "1.0.1"),
    ],
    targets: [
        .target(
            name: "MancalaCore",
            dependencies: ["GamingCore"]),
        .testTarget(
            name: "MancalaCoreTests",
            dependencies: ["MancalaCore"])
    ]
)
